import ReadHDF5
import numpy as np

def process():

    filename = '/data_cellar/cloudprojects/data/GOES-16-TDS/20180614/OR_ABI-L1b-RadC-M3C16_G16_s20181652352221_e20181652355005_c20181652355042.nc'
    data, metadata = ReadHDF5.get_data(filename, dump=True)
    return data, metadata

def extrapolateLatLon(data, metadata):

    if 'x' in keys:
            x_num_points = len(data['x'])
    if 'y' in keys:
            y_num_points = len(data['y'])
    if 'x_image_bounds' in keys:
            x_bounds = data['x_image_bounds']
    if 'y_image_bounds' in keys:
            y_bounds = data['y_image_bounds']

    x = np.linspace(x_bounds[0], x_bounds[1], x_num_points)
    y = np.linspace(y_bounds[0], y_bounds[1], y_num_points)
    x = x.reshape((len(x), 1))
    x = np.tile(x, y_num_points)
    y = np.tile(y, (x_num_points,1))
    lon, lat = angles2geo(x, y, metadata['geospatial_lat_lon_extent']['geospatial_lon_nadir'])
    data['Latitude'] = lat
    data['Longitude'] = lon

def cart_to_coord(ii,jj,slon):
    # (based on code from Ted Kennelly)
    # Convert Full Disk I/J Pixel location in the GOES-R Fixed Grid Projection to Lat/Lon
    # Inputs: Pixel I, Pixel J, and satellite nadir longitude

    #if sat == 0: slon = np.double(-137.0) # west
    #if sat == 1: slon = np.double(-89.5)  # test
    #if sat == 2: slon = np.double(-75.0)  # east

    # Constants
    h = np.double(42164167.)
    rpol = np.double(6356752.31414)
    requ = np.double(6378137.0)

    # Scale and offset for ABI 2km grid
    sx =  0.000056
    sy = -0.000056
    ax = -0.151844
    ay =  0.151844

    # convert I/J to sensor angles
    x = sx*ii+ax
    y = sy*jj+ay

    # Fixed Grid conversion sensor angles to lat/lon
    a = pow(np.sin(x),2) + pow(np.cos(x),2) * (pow(np.cos(y),2) + pow(requ,2)/pow(rpol,2)*pow(np.sin(y),2))
    b = -2.0*h*np.cos(x) * np.cos(y)
    c = pow(h,2) - pow(requ,2)

    # Avoid runtime warning (sqrt of a negative value). These will be replaced with fill values below.
    numerator = pow(b,2)-4*a*c
    numerator[np.where(numerator<0)] = 0

    rs = (-1.*b-np.sqrt(numerator))/(2.*a)
    sx = rs*np.cos(x) * np.cos(y)
    sy = rs*np.sin(x)
    sz = rs*np.cos(x) * np.sin(y)

    lat = np.arctan( (pow(requ,2)/pow(rpol,2)) * sz / (np.sqrt(pow((h-sx),2)+pow(sy,2)))) * 180/np.pi
    lon = np.arctan(sy/(h-sx))*180/np.pi
    lon = lon + slon

    # set non-visible locations to default value
    wb0 = np.where((pow(b,2)-4*a*c) < 0.)[0]
    wb1 = np.where((pow(b,2)-4*a*c) < 0.)[1]
    nb = wb0.size
    fill = -999
    if nb != 0: lon[wb0,wb1] = fill
    if nb != 0: lat[wb0,wb1] = fill

    return lon,lat

### geo2sat :: Convert Lat/Lon to Full Disk Pixel I/J
def coord_to_cart(lon,lat,slon):

  # Convert Lon/Lat to Full Disk I/J Pixel location in the GOES-R Fixed Grid Projection
  # Inputs: longitude, latitude, and satellite specification (0,1,2)

  #if sat == 0: slon = np.double(-137.0) # west
  #if sat == 1: slon = np.double(-89.5)  # test
  #if sat == 2: slon = np.double(-75.0)  # east

  # Constants
  h = np.double(42164167.)
  rpol = np.double(6356752.31414)
  requ = np.double(6378137.0)

  # Fixed Grid Conversion from lat/lon to sensor angles (theta, lambd)
  c1 = pow(rpol,2)/pow(requ,2)
  c2 = (pow(requ,2)-pow(rpol,2))/pow(requ,2)
  geocenlat = np.arctan(c1*np.tan(lat/180.*np.pi))
  re = rpol/np.sqrt(1.-c2*np.cos(geocenlat)*np.cos(geocenlat))
  r1 = h - re*np.cos(geocenlat)*np.cos((lon-slon)/180.*np.pi)
  r2 = -1.0 * re*np.cos(geocenlat)*np.sin((lon-slon)/180.*np.pi)
  r3 = re*np.sin(geocenlat)
  lambd = np.arcsin(-1.*r2/np.sqrt(pow(r1,2)+pow(r2,2)+pow(r3,2)))
  theta = np.arctan(r3/r1)

  # Scale and offset for ABI 2 km grid
  NPIX = 5424
  aa = np.double(0.000056)
  bb = (np.double(NPIX)/2 - 0.5)

  # Convert sensor angles to pixel coordinates
  ii = lambd/aa+bb
  jj = (theta/(-1*aa)+bb)

  return ii,jj


def angles2geo(x, y, slon):

    h = np.double(42164167.)
    rpol = np.double(6356752.31414)
    requ = np.double(6378137.0)

    a = pow(np.sin(x),2) + pow(np.cos(x),2) * (pow(np.cos(y),2) + pow(requ,2)/pow(rpol,2)*pow(np.sin(y),2))
    b = -2.0*h*np.cos(x) * np.cos(y)
    c = pow(h,2) - pow(requ,2)

    # Avoid runtime warning (sqrt of a negative value). These will be replaced with fill values below.
    numerator = pow(b,2)-4*a*c
    numerator[np.where(numerator<0)] = 0

    rs = (-1.*b-np.sqrt(numerator))/(2.*a)
    sx = rs*np.cos(x) * np.cos(y)
    sy = rs*np.sin(x)
    sz = rs*np.cos(x) * np.sin(y)

    lat = np.arctan( (pow(requ,2)/pow(rpol,2)) * sz / (np.sqrt(pow((h-sx),2)+pow(sy,2)))) * 180/np.pi
    lon = np.arctan(sy/(h-sx))*180/np.pi
    lon = lon + slon

    # set non-visible locations to default value
    wb0 = np.where((pow(b,2)-4*a*c) < 0.)[0]
    wb1 = np.where((pow(b,2)-4*a*c) < 0.)[1]
    nb = wb0.size
    fill = -999
    if nb != 0: lon[wb0,wb1] = fill
    if nb != 0: lat[wb0,wb1] = fill

    return lon,lat
