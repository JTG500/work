import numpy as np
import h5py

def get_data(filename, keys=None, parent_groups=None, child_groups=None, attributes=None, dump=False):

    data = {}
    hdf_file = h5py.File(filename, 'r')
    parent = {}
    child = {}
    metadata = {}

    if parent_groups is not None:
        for parent_group in parent_groups:
            if parent_group in hdf_file.keys():
                parent[parent_group] = hdf_file.get(parent_group)
            if child_groups is not None:
                for child_group in child_groups:
                    if child_group in parent[parent_group].keys():
                        child[child_group] = parent[parent_group].get(child_group)
                        for key in keys:
                            if key in child[child_group].keys():
                                data[child_group+'('+key+')'] = child[child_group][key].value
            else:
                for key in keys:
                    if key in parent[parent_group].keys():
                        data[parent_group+'('+key+')'] = parent[parent_group][key].value
    else:
        if keys is not None and attributes is None:
            for key in keys:
                data[key] = hdf_file[key].value

        elif keys is not None and attributes is not None:
            for key in keys:
                metadata[key] = {}
                data[key] = hdf_file[key].value
                for attribute in attributes:
                    if attribute in hdf_file[key].attrs.keys():
                        metadata[key][attribute] = hdf_file[key].attrs.get(attribute)
        elif keys is None and not dump:
            data = None
        else:
            for key, value in hdf_file.iteritems():
                metadata[key] = {}
                data[key] = hdf_file[key].value
                for attribute in hdf_file[key].attrs.keys():
                    metadata[key][attribute] = hdf_file[key].attrs.get(attribute)
            metadata['entry'] = {}
            for key in hdf_file.attrs.keys():
                metadata['entry'][key] = hdf_file.attrs.get(key)

    hdf_file.close()
    if keys is not None and attributes is not None or dump:
        return data, metadata
    else:
        return data
