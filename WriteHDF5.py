import h5py
import numpy as np
import os

#TODO write a test case that confirms the overwriting to a file and appending
def write_hdf5(filename, dict_inputs, flag='w', verbose=0):

    flag = flag.lower()

    if isinstance(filename, str) == False and isinstance(filename, np.string_) == False:
        raise TypeError("Filename is not a valid type. Must be of type string, received type: ", type(filename))

    if isinstance(dict_inputs, dict) == False:
        raise TypeError("Parameter 2 must be of type dict, received type: ", type(dict_inputs))

    if isinstance(flag, str) == False and isinstance(flag, np.string_) == False:
        raise TypeError("Optional Parameter flag must be of type string, received type: ", type(flag))

    if flag != 'w' and flag != 'a':
        raise ValueError("Optional Parameter flag must be either 'w' or 'a', received: ", flag)

    check_file_exist = os.path.isfile(filename)

    if flag == 'a' and check_file_exist == False:
        raise FileNotFoundError("Optional parameter 'flag': ", flag, " Can not append to file ", filename, " file does not exist!")

    hf = h5py.File(filename, flag)

    for key, value in dict_inputs.iteritems():
        hf.create_dataset(key, data=value)
        if verbose == 1:
            print "Key named: ", key, " has been written to file: ", filename
        if verbose == 2:
            value = np.asarray(value)
            if value.shape is ():
                value.shape = 1
            print "Key named: ", key, " with shape of ", value.shape, " has been written to file: ", filename
        if verbose == 3:
            value = np.asarray(value)
            if value.shape is ():
                value.shape = 1
            print "key named: ", key, " with shape of ", value.shape, " and datatype of: ", value.dtype, " has been written to file: ", filename

    hf.close()
