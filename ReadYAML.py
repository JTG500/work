import yaml

def get_data(filename, keys=None, return_single=False):

    data = {}
    yaml_file = file(filename, 'r')
    yaml_data = yaml.load(yaml_file)
    if keys is None:
        yaml_file.close()
        return yaml_data

    if return_single == False:
        for key in keys:
            data[key] = yaml_data[key]

        yaml_file.close()
        return data

    else:
        data = yaml_data[keys[0]]
        yaml_file.close()

        return data
