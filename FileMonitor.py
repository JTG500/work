import ReadYAML

class Monitor(object):

    def __init__(self, configFile):

        config = ReadYAML.get_data(configFile)
        self.ABI_dir = config['inputs']['ABI_dir']
        self.HIM_dir = config['inputs']['HIM_dir']
        self.MET_dir = config['inputs']['MET_dir']
        self.MVI_dir = config['inputs']['MVI_dir']
        self.SEV_dir = config['inputs']['SEV_dir']
        self.SVI_dir = config['inputs']['SVI_dir']
        self.MOS_dir = config['inputs']['MOS_dir']
        self._listen()

    def _listen(self):

        print self.ABI_dir
        self._signal()

    def _signal(self):
